# [CosmicPaella](https://bitbucket.org/steog88/cosmicpaella_public/)
by S. Gariazzo (stefano.gariazzo@gmail.com)

Universe content after Planck 2018 results, in a pie-chart like plot, for outreach purposes mainly in Valencia territory.  
The simple pie chart and the cosmic paella share the same format for easy use in talks or similar.

The pie charts use the [Planck 2018 results](https://arxiv.org/abs/1807.06209) on the energy density of dark energy, dark matter and baryons, extracted using the "*Planck* TT,TE,EE + lowE + lensing + BAO" data combination.

## Acknowledgements
Paella images are taken from the web.  
[Planck](https://www.esa.int/Planck) is a project of the European Space Agency (ESA).  
This repository is part of a project that has received funding from the European Union's Horizon 2020 research and innovation programme under the Marie Skłodowska-Curie grant agreement No [796941 (ENCORE)](https://encore.astroparticles.es/).
